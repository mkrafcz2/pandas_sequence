import pandas as pd

def contiguous_group_indices(df_or_series, sequence_index_level=None, sequence_col=None, sequence_function=None):
    """
    Produce series of continuous group labels for a given sequence column and sequencing function

    Suppose a column or index level contains a 'sequencable value'. This may be any
    value with a countable number of discrete elements which may be ordered 'by 1'.

    This function produces a series (with index matching the df) containing integers indicating
    contiguous groups of values. This is useful for proper column shift logic.

    Ex:
    Consider the following DataFrame of Security prices. It has a multi-level
    index whose second level is a sequencable value 'Quarter'.

    Security Quarter Price
    Sec-1    2019Q1  10.
             2019Q2  11.
             2019Q4  10.5
             2020Q1  10.6
             2020Q2  10.7
    Sec-2    2018Q1  25
             2018Q2  24
             2018Q3  25
             2018Q4  26
             2019Q2  20

    Passing sequence_index_level='Quarter' and a sequence function like yq_diff:
    def yr(quarter):
        return int(quarter[:4])
    def mon(quarter):
        return int(quarter[5:])
    def yq_diff(yq, yq_ref):
        return ((yr(yq)*4+mon(yq))-((yr(yq_ref)*4)+mon(yq_ref)))

    grp_ids = contiguous_group_indices(df, sequence_index_level='Quarter', sequence_function=yq_diff)

    Yields grp_ids as:

    Security Quarter
    Sec-1    2019Q1  1
             2019Q2  1
             2019Q4  2
             2020Q1  2
             2020Q2  2
    Sec-2    2018Q1  3
             2018Q2  3
             2018Q3  3
             2018Q4  3
             2019Q2  4

    The series indicates groups of contiguous values. We can find differences in price to
    previous quarters properly respecting gaps when they pop up.

    df['Price-diff'] = df['Price']-df['Price'].groupby(grp_ids).shift(1)
    """

    # Fetch sequence series
    sequence_series = None
    if type(df_or_series) is pd.DataFrame:
        if sequence_col is not None:
            sequence_series = df_or_series.loc[:,sequence_col]
        elif sequence_index_level is not None:
            if sequence_index_level not in df_or_series.index.names:
                raise ValueError(f"sequence_index_level {sequence_index_level} not in the data frame. available levels: {df_or_series.index.names}")
            level_idx = df_or_series.index.names.index(sequence_index_level)
            sequence_series = df_or_series.index.to_series().apply(lambda t: t[level_idx])
        else:
            # Get the dataframe index
            sequence_series = df_or_series.index.to_series()
    elif type(df_or_series) is pd.core.series.Series:
        sequence_series = df_or_series
    else:
        try:
            # Fallback assuming we might have an index. If we do this will succeed.
            sequence_series = df_or_series.to_series()
        except AttributeError:
            raise TypeError(f"You must pass a pandas series, index, or dataframe as df_or_series. Got type {type(df_or_series)}")

    # If the sequence function is None, set it as the simple difference formula
    if sequence_function is None:
        sequence_function_ = lambda s: s-ref_val
    else:
        sequence_function_ = lambda s: sequence_function(s, ref_val)

    # Compute differences against 'reference' value
    ref_val = sequence_series.iloc[0]
    try:
        sequence_values = sequence_series.apply(sequence_function_)
    except Exception as e:
        print(f"Tried to subtract sequence values but ran into an error!")
        raise e

    # Check that sequence is an integer type
    if not pd.api.types.is_integer_dtype(sequence_values.dtype):
        raise TypeError(f"Sequence value type: {sequence_values.dtype} is not an integer type!")

    # Detect sequential groups
    S = (sequence_values-sequence_values.shift(1)).fillna(0.0).astype(int)

    # Group ids
    # This procedure may fail if the selected index level isn't the 'lowest'.
    grp_ids = (S != 1).cumsum()

    return grp_ids

def sequence_df(df, lags, group_specs=None):
    """
    Sequence feature data into multi-component rows.

    This function takes a dataframe containing various features over a sequence. These
    are then stacked so neighboring values can be easily accessed by a specific sequence value.

    The input dataframe should have the following structure:

    Sequence | 'Feat 1' | 'Feat 2' |
    s1       | f1(s1)   | f2(s1)   |
    s2       | f1(s2)   | f2(s2)   |
    ...

    The function then returns for a set of lags:

    Sequence | ('Feat 1' , -2) | ('Feat 2', -2) | ('Feat 1', -1) | ('Feat 2', -1) | ('Feat 1', 0) | ('Feat 2', 0) |
    s3       | f1(s1)          | f2(s1)         | f1(s2)         | f2(s2)         | f1(s3)        | f2(s3)        |
    s4       | f1(s2)          | f2(s2)         | f1(s3)         | f2(s3)         | f1(s4)        | f2(s4)        |
    ...

    if res_df is the result dataframe, then for many models, the X matrix is simply:
    res_df.values[:,:num_days]

    Named Arguments
      df: A Pandas dataframe containing a set of features for each day
      lags: A list of lags to include
      group_specs: A list of tuples defining how groups are discovered. (Can also be None or empty list)
        'group' type specs - Group type specs specify columns, or index levels where the groups are already defined.
        'sequence' type specs - Sequence type specs specify 'sequencable' columns. These columns have a 'by-one' well ordering defined.
            This well ordering can either be implicit if you use integers, or you can pass a function which defines it.
        'level' subtype specs - These specs indicate that the data passed indicate a specific level of the data frame's index should be used.
        'column' subtype specs - These specs indicate that the data passed with the spec is a column of some type. Either a name or a column type.
        'index' subtype specs - sequence specs also support the index subtype. This indicates to just use the index of the data frame.
        A spec is specified like so: (<type>, <subtype>, data, [<sequence_function>])

        A few examples:
        ('group', 'level', 'Security') - Use the 'Security' index level as a pre-defined grouping
        ('sequence', 'level', 'Date', days_diff) - Use the 'Date' index level as a sequencable column to define a grouping. Use the days_diff function to define the order
        ('sequence', 'index') - Use the index of the dataframe as a sequencable column. Since no function is specified, it will just use arithmetic.
        ('group', 'column', groups) - Use the groups series to define the groups to use. This is a column passed in.
        ('group', 'column', 'Group') - Use the 'Group' column of the dataframe to define the groups to use. This is a column passed in.

        If group_specs is None or an empty list, no grouping is done, and the lags are computed using shift directly on the input DataFrame.

    returns
      A pandas dataframe containing rows of prediction and/or label data.
    """

    if group_specs is None:
        group_specs = []

    by = []
    level = []
    remove_columns = []
    for spec in group_specs:
        if len(spec) < 2:
            raise ValueError("Group specs must contain at least three elements")
        if spec[0] == 'group':
            # These are group type specs
            if spec[1] == 'level':
                if spec[2] not in df.index.names:
                    raise ValueError(f"Level name {spec[2]} not found in index!")
                level.append(spec[2])
            elif spec[1] == 'column':
                if type(spec[2]) is pd.core.series.Series:
                    by.append(spec[2])
                else:
                    if spec[2] not in df.columns:
                        raise ValueError(f"Column name {spec[2]} not found!")
                    by.append(df[spec[2]])
                    remove_columns.append(spec[2])
            else:
                raise ValueError(f"Group subtype {spec[1]} not supported")
        elif spec[0] == 'sequence':
            # These are the sequence type specs
            if spec[1] == 'level':
                if spec[2] not in df.index.names:
                    raise ValueError(f"Level name {spec[2]} not found in index!")
                if len(spec) == 4:
                    if not callable(spec[3]):
                        raise ValueError(f"The fourth element of a group spec must be a callable!")
                    g_ids = contiguous_group_indices(df, sequence_index_level=spec[2], sequence_function=spec[3])
                else:
                    g_ids = contiguous_group_indices(df, sequence_index_level=spec[2])
                by.append(g_ids)
            elif spec[1] == 'column':
                if type(spec[2]) is pd.core.series.Series:
                    if len(spec) == 4:
                        if not callable(spec[3]):
                            raise ValueError(f"The fourth element of a group spec must be a callable!")
                        g_ids = contiguous_group_indices(spec[2], sequence_function=spec[3])
                    else:
                        g_ids = contiguous_group_indices(spec[2])
                else:
                    if spec[2] not in df.columns:
                        raise ValueError(f"Column name {spec[2]} not found!")
                    remove_columns.append(spec[2])
                    if len(spec) == 4:
                        if not callable(spec[3]):
                            raise ValueError(f"The fourth element of a group spec must be a callable!")
                        g_ids = contiguous_group_indices(df, sequence_col=spec[2], sequence_function=spec[3])
                    else:
                        g_ids = contiguous_group_indices(df, sequence_col=spec[2])
                by.append(g_ids)
            elif spec[1] == 'index':
                if len(spec) == 3:
                    if not callable(spec[2]):
                        raise ValueError(f"The third element of an index group spec must be a callable!")
                    g_ids = contiguous_group_indices(df, sequence_function=spec[2])
                else:
                    g_ids = contiguous_group_indices(df)
                by.append(g_ids)
            else:
                raise ValueError(f"Group subtype {spec[1]} not supported")
        else:
            raise ValueError(f"Group spec of type {spec[0]} not supported")

    # Remove columns
    temp_df = df.loc[:,list(filter(lambda c: c not in remove_columns, df.columns))]

    if len(level) == 0:
        level = None
    if len(by) == 0:
        by = None

    # There's a bug where if both by and level are passed to groupby, it throws an error:
    # TypeError: 'numpy.ndarray' object is not callable
    # We need to mitigate this by detecting if both by and level are non-zero and if so, transition the level values
    # to columns and add them to by.

    if (level is not None) and (by is not None):
        for lvl in level:
            lvl_idx = df.index.names.index(lvl)
            lvl_vals = df.index.to_series().apply(lambda t: t[lvl_idx])
            by.append(lvl_vals)
        level = None

    # Change column types to support nans
    integer_columns = temp_df.dtypes[temp_df.dtypes.apply(pd.api.types.is_integer_dtype)]
    for col_name in integer_columns.index:
        temp_df.loc[:,[col_name]] = temp_df.loc[:,[col_name]].astype(pd.Int64Dtype())

    # Strip multiindex columns if needed
    if (by is None) and (level is None):
        # No need to group the DataFrame here.
        temp_gbydf = temp_df
    else:
        # Produce Groupby DataFrame
        temp_gbydf = temp_df.groupby(by=by, level=level, axis=0)

    dfs = []

    for lag in lags:
        slice_df = temp_gbydf.shift(-lag)

        if slice_df is not None:
            if len(slice_df) == 0:
                raise ValueError(f"No data at lag {lag}")
            slice_df.columns = pd.MultiIndex.from_product([slice_df.columns.tolist(),[lag]])
            dfs.append(slice_df)

    # Join segments into full dataframe.
    DF = pd.concat(dfs, axis=1, join='outer').dropna()

    # Restore the original types of the integer columns
    for col in DF.columns:
        if col[0] in integer_columns.index:
            DF[col] = DF[col].astype(integer_columns.loc[col[0]])

    return DF
