from .sequence import sequence_df, contiguous_group_indices

__version__ = "0.3.1"

__all__ = [ "sequence_df", "contiguous_group_indices" ]
