from setuptools import setup

setup(
    name = "pandas_sequence",
    version = "0.3.1",
    author = "Matthew Krafczyk",
    author_email = "krafczyk.matthew@gmail.com",
    description = ("A sequence building utility library"),
    #license = "MIT",
    #url =,
    packages=['pandas_sequence'],
    install_requires = [
        'pandas',
    ],
)
