import pandas as pd
import pandas_sequence as pds
import numpy as np
import pytest

def test_basic_group_col_1_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1,1],
         [1,2],
         [1,3],
         [1,4],
         [2,1],
         [2,2],
         [2,3],
         [3,1]],
        columns=['Group', 'Value'],
        index=[0,1,2,3,4,5,6,7]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'column', 'Group')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_group_col_1_2():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A',1],
         ['A',2],
         ['A',3],
         ['A',4],
         ['B',1],
         ['B',2],
         ['B',3],
         ['C',1]],
        columns=['Group', 'Value'],
        index=[0,1,2,3,4,5,6,7]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'column', 'Group')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_group_col_1_3():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1],
         [2],
         [3],
         [4],
         [1],
         [2],
         [3],
         [1]],
        columns=['Value'],
        index=[0,1,2,3,4,5,6,7]
    )
    group_col = pd.Series([1,1,1,1,2,2,2,3])

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'column', group_col)])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1],
         [2],
         [3],
         [4],
         [1],
         [2],
         [3],
         [1]],
        columns=['Value'],
        index=[0,1,2,3,5,6,7,9]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('sequence', 'index')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,6,7])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_group_index_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1],
         [2],
         [3],
         [4],
         [1],
         [2],
         [3],
         [1]],
        columns=['Value'],
        index=pd.MultiIndex.from_tuples([
            ('A', 0),
            ('A', 1),
            ('A', 2),
            ('A', 3),
            ('B', 4),
            ('B', 5),
            ('B', 6),
            ('C', 7)],
            names=['Group', 'count'])
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'level', 'Group')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=pd.MultiIndex.from_tuples([
            ('A', 1),
            ('A', 2),
            ('A', 3),
            ('B', 5),
            ('B', 6)],
            names=['Group', 'count']))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_sequence_index_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1],
         [2],
         [3],
         [4],
         [1],
         [2],
         [3],
         [1]],
        columns=['Value'],
        index=pd.MultiIndex.from_tuples([
            ('A', 0),
            ('A', 1),
            ('A', 2),
            ('A', 3),
            ('B', 0),
            ('B', 1),
            ('B', 2),
            ('C', 0)],
            names=['Group', 'count'])
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('sequence', 'level', 'count')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=pd.MultiIndex.from_tuples([
            ('A', 1),
            ('A', 2),
            ('A', 3),
            ('B', 1),
            ('B', 2)],
            names=['Group', 'count']))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_group_col_2_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1,1],
         [1,2],
         [1,3],
         [1,4],
         [2,5],
         [2,6],
         [2,7],
         [3,8]],
        columns=['Group', 'Value'],
        index=[0,1,2,3,4,5,6,7]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'column', 'Group')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [5,6],
         [6,7]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_group_col_2_2():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A',1],
         ['A',2],
         ['A',3],
         ['A',4],
         ['B',5],
         ['B',6],
         ['B',7],
         ['C',8]],
        columns=['Group', 'Value'],
        index=[0,1,2,3,4,5,6,7]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'column', 'Group')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [5,6],
         [6,7]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_sequence_col_1_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1,1],
         [2,2],
         [3,3],
         [4,4],
         [1,1],
         [2,2],
         [3,3],
         [1,1]],
        columns=['Seq', 'Value'],
        index=[0,1,2,3,4,5,6,7]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('sequence', 'column', 'Seq')])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_sequence_col_1_2():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A',1],
         ['B',2],
         ['C',3],
         ['D',4],
         ['A',1],
         ['B',2],
         ['C',3],
         ['A',1]],
        columns=['Seq', 'Value'],
        index=[0,1,2,3,4,5,6,7]
    )

    def str_diff(s, ref_val):
        return ord(s)-ord(ref_val)

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('sequence', 'column', 'Seq', str_diff)])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_basic_sequence_col_1_3():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1],
         [2],
         [3],
         [4],
         [1],
         [2],
         [3],
         [1]],
        columns=['Value'],
        index=[0,1,2,3,4,5,6,7]
    )

    sequence_col = pd.Series(['A', 'B', 'C', 'D', 'A', 'B', 'C', 'A'])

    def str_diff(s, ref_val):
        return ord(s)-ord(ref_val)

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('sequence', 'column', sequence_col, str_diff)])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([['Value'],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_large_1_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A', '2020-01-01', 0.2],
         ['A', '2020-01-02', -0.1],
         ['A', '2020-01-03', 0.345],
         ['A', '2020-01-04', 0.55],
         ['A', '2020-01-05', 1.2],
         ['B', '2020-01-01', -2.5],
         ['B', '2020-01-02', 3.7],
         ['B', '2020-01-03', 3.5],
         ['B', '2020-01-04', 0.3],
         ['B', '2020-01-05', -1.],
         ['C', '2020-01-01', -2.1],
         ['C', '2020-01-02', 1.1],
         ['C', '2020-01-03', 1.123],
         ['C', '2020-01-04', 5.3],
         ['D', '2020-01-01', 5.55]],
        columns=['Group', 'Date', 'Value'],
    )
    df = df.set_index(['Group', 'Date'])

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'level', 'Group')])

    target_df = pd.DataFrame(
        [['A', '2020-01-02', 0.2, -0.1],
         ['A', '2020-01-03', -0.1, 0.345],
         ['A', '2020-01-04', 0.345, 0.55],
         ['A', '2020-01-05', 0.55, 1.2],
         ['B', '2020-01-02', -2.5, 3.7],
         ['B', '2020-01-03', 3.7, 3.5],
         ['B', '2020-01-04', 3.5, 0.3],
         ['B', '2020-01-05', 0.3, -1.],
         ['C', '2020-01-02', -2.1, 1.1],
         ['C', '2020-01-03', 1.1, 1.123],
         ['C', '2020-01-04', 1.123, 5.3]],
        columns=['Group', 'Date', ('Value', -1), ('Value', 0)],
    )
    target_df = target_df.set_index(['Group', 'Date'])
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_large_1_2():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A', '2020-01-01', 0.2],
         ['A', '2020-01-02', -0.1],
         ['A', '2020-01-03', 0.345],
         ['A', '2020-01-04', 0.55],
         ['A', '2020-01-05', 1.2],
         ['B', '2020-01-01', -2.5],
         ['B', '2020-01-02', 3.7],
         ['B', '2020-01-03', 3.5],
         ['B', '2020-01-04', 0.3],
         ['B', '2020-01-05', -1.],
         ['C', '2020-01-01', -2.1],
         ['C', '2020-01-02', 1.1],
         ['C', '2020-01-03', 1.123],
         ['C', '2020-01-04', 5.3],
         ['D', '2020-01-01', 5.55]],
        columns=['Group', 'Date', 'Value'],
    )
    df = df.set_index(['Group', 'Date'])

    sequenced_df = pds.sequence_df(df, [-2, 0, 1], group_specs=[('group', 'level', 'Group')])

    target_df = pd.DataFrame(
        [['A', '2020-01-03', 0.2, 0.345, 0.55],
         ['A', '2020-01-04', -0.1, 0.55, 1.2],
         ['B', '2020-01-03', -2.5, 3.5, 0.3],
         ['B', '2020-01-04', 3.7, 0.3, -1.],
         ['C', '2020-01-03', -2.1, 1.123, 5.3]],
        columns=['Group', 'Date', ('Value', -2), ('Value', 0), ('Value', 1)],
    )
    target_df = target_df.set_index(['Group', 'Date'])
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_large_1_3():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A', '2020-01-01', 0.2],
         ['A', '2020-01-02', -0.1],
         ['A', '2020-01-03', 0.345],
         ['A', '2020-01-04', 0.55],
         ['A', '2020-01-05', 1.2],
         ['B', '2020-01-01', -2.5],
         ['B', '2020-01-02', 3.7],
         ['B', '2020-01-03', 3.5],
         ['B', '2020-01-04', 0.3],
         ['B', '2020-01-05', -1.],
         ['C', '2020-01-01', -2.1],
         ['C', '2020-01-02', 1.1],
         ['C', '2020-01-03', 1.123],
         ['C', '2020-01-04', 5.3],
         ['D', '2020-01-01', 5.55]],
        columns=['Group', 'Date', 'Value'],
    )
    df = df.set_index(['Group', 'Date'])

    sequenced_df = pds.sequence_df(df, [0,-1], group_specs=[('group', 'level', 'Group')])

    target_df = pd.DataFrame(
        [['A', '2020-01-02', -0.1, 0.2],
         ['A', '2020-01-03', 0.345, -0.1],
         ['A', '2020-01-04', 0.55, 0.345],
         ['A', '2020-01-05', 1.2, 0.55],
         ['B', '2020-01-02', 3.7, -2.5],
         ['B', '2020-01-03', 3.5, 3.7],
         ['B', '2020-01-04', 0.3, 3.5],
         ['B', '2020-01-05', -1., 0.3],
         ['C', '2020-01-02', 1.1, -2.1],
         ['C', '2020-01-03', 1.123, 1.1],
         ['C', '2020-01-04', 5.3, 1.123]],
        columns=['Group', 'Date', ('Value', 0), ('Value', -1)],
    )
    target_df = target_df.set_index(['Group', 'Date'])
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_large_1_4():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A', '2020-01-01', 0.2],
         ['A', '2020-01-02', -0.1],
         ['A', '2020-01-03', 0.345],
         ['A', '2020-01-04', 0.55],
         ['A', '2020-01-05', 1.2],
         ['B', '2020-01-01', -2.5],
         ['B', '2020-01-02', 3.7],
         ['B', '2020-01-03', 3.5],
         ['B', '2020-01-04', 0.3],
         ['B', '2020-01-05', -1.],
         ['C', '2020-01-01', -2.1],
         ['C', '2020-01-02', 1.1],
         ['C', '2020-01-03', 1.123],
         ['C', '2020-01-04', 5.3],
         ['D', '2020-01-01', 5.55]],
        columns=['Group', 'Date', 'Value'],
    )
    df = df.set_index(['Group', 'Date'])

    sequenced_df = pds.sequence_df(df, [-2, -1, 0], group_specs=[('group', 'level', 'Group')])

    target_df = pd.DataFrame(
        [['A', '2020-01-03', 0.2, -0.1, 0.345],
         ['A', '2020-01-04', -0.1, 0.345, 0.55],
         ['A', '2020-01-05', 0.345, 0.55, 1.2],
         ['B', '2020-01-03', -2.5, 3.7, 3.5],
         ['B', '2020-01-04', 3.7, 3.5, 0.3],
         ['B', '2020-01-05', 3.5, 0.3, -1.],
         ['C', '2020-01-03', -2.1, 1.1, 1.123],
         ['C', '2020-01-04', 1.1, 1.123, 5.3]],
        columns=['Group', 'Date', ('Value', -2), ('Value', -1), ('Value', 0)],
    )
    target_df = target_df.set_index(['Group', 'Date'])
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_large_1_5():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A', '2020-01-01', 0.2],
         ['A', '2020-01-02', -0.1],
         ['A', '2020-01-04', 0.345],
         ['A', '2020-01-05', 0.55],
         ['A', '2020-01-06', 1.2],
         ['B', '2020-01-07', -2.5],
         ['B', '2020-01-08', 3.7],
         ['B', '2020-01-10', 3.5],
         ['B', '2020-01-11', 0.3],
         ['B', '2020-01-13', -1.],
         ['C', '2020-01-01', -2.1],
         ['C', '2020-01-02', 1.1],
         ['C', '2020-01-03', 1.123],
         ['C', '2020-01-04', 5.3],
         ['D', '2020-01-05', 5.55]],
        columns=['Group', 'Date', 'Value'],
    )
    df['Date'] = pd.to_datetime(df['Date'])
    df = df.set_index(['Group', 'Date'])

    def days_diff(d, ref_val):
        return (d-ref_val).days

    sequenced_df = pds.sequence_df(df, [-1, 0], group_specs=[('group', 'level', 'Group'), ('sequence', 'level', 'Date', days_diff)])

    target_df = pd.DataFrame(
        [['A', '2020-01-02', 0.2, -0.1],
         ['A', '2020-01-05', 0.345, 0.55],
         ['A', '2020-01-06', 0.55, 1.2],
         ['B', '2020-01-08', -2.5, 3.7],
         ['B', '2020-01-11', 3.5, 0.3],
         ['C', '2020-01-02', -2.1, 1.1],
         ['C', '2020-01-03', 1.1, 1.123],
         ['C', '2020-01-04', 1.123, 5.3]],
        columns=['Group', 'Date', ('Value', -1), ('Value', 0)],
    )
    target_df['Date'] = pd.to_datetime(target_df['Date'])
    target_df = target_df.set_index(['Group', 'Date'])
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_large_1_6():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A', '2020-01-01', 0.2],
         ['A', '2020-01-02', -0.1],
         ['A', '2020-01-04', 0.345],
         ['A', '2020-01-05', 0.55],
         ['A', '2020-01-06', 1.2],
         ['B', '2020-01-07', -2.5],
         ['B', '2020-01-08', 3.7],
         ['B', '2020-01-10', 3.5],
         ['B', '2020-01-11', 0.3],
         ['B', '2020-01-13', -1.],
         ['C', '2020-01-01', -2.1],
         ['C', '2020-01-02', 1.1],
         ['C', '2020-01-03', 1.123],
         ['C', '2020-01-04', 5.3],
         ['D', '2020-01-05', 5.55]],
        columns=['Group', 'Date', 'Value'],
    )
    df['Date'] = pd.to_datetime(df['Date'])
    df = df.set_index(['Group', 'Date'])

    def days_diff(d, ref_val):
        return (d-ref_val).days

    sequenced_df = pds.sequence_df(df, [-1, 0], group_specs=[('sequence', 'level', 'Date', days_diff)])

    target_df = pd.DataFrame(
        [['A', '2020-01-02', 0.2, -0.1],
         ['A', '2020-01-05', 0.345, 0.55],
         ['A', '2020-01-06', 0.55, 1.2],
         ['B', '2020-01-07', 1.2, -2.5],
         ['B', '2020-01-08', -2.5, 3.7],
         ['B', '2020-01-11', 3.5, 0.3],
         ['C', '2020-01-02', -2.1, 1.1],
         ['C', '2020-01-03', 1.1, 1.123],
         ['C', '2020-01-04', 1.123, 5.3],
         ['D', '2020-01-05', 5.3, 5.55]],
        columns=['Group', 'Date', ('Value', -1), ('Value', 0)],
    )
    target_df['Date'] = pd.to_datetime(target_df['Date'])
    target_df = target_df.set_index(['Group', 'Date'])
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_nogrouping_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [['2020-01-01', 0.2],
         ['2020-01-02', -0.1],
         ['2020-01-04', 0.345],
         ['2020-01-05', 0.55],
         ['2020-01-06', 1.2],
         ['2020-01-07', -2.5],
         ['2020-01-08', 3.7],
         ['2020-01-10', 3.5],
         ['2020-01-11', 0.3],
         ['2020-01-13', -1.],
         ['2020-01-01', -2.1],
         ['2020-01-02', 1.1],
         ['2020-01-03', 1.123],
         ['2020-01-04', 5.3],
         ['2020-01-05', 5.55]],
        columns=['Date', 'Value'],
    )
    df['Date'] = pd.to_datetime(df['Date'])
    df = df.set_index('Date')

    sequenced_df = pds.sequence_df(df, [-1, 0])

    target_df = pd.DataFrame(
         [['2020-01-02', 0.2, -0.1],
          ['2020-01-04', -0.1, 0.345],
          ['2020-01-05', 0.345, 0.55],
          ['2020-01-06', 0.55, 1.2],
          ['2020-01-07', 1.2, -2.5],
          ['2020-01-08', -2.5, 3.7],
          ['2020-01-10', 3.7, 3.5],
          ['2020-01-11', 3.5, 0.3],
          ['2020-01-13', 0.3, -1.],
          ['2020-01-01', -1., -2.1],
          ['2020-01-02', -2.1, 1.1],
          ['2020-01-03', 1.1, 1.123],
          ['2020-01-04', 1.123, 5.3],
          ['2020-01-05', 5.3, 5.55]],
        columns=['Date', ('Value', -1), ('Value', 0)],
    )
    target_df['Date'] = pd.to_datetime(target_df['Date'])
    target_df = target_df.set_index('Date')
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_nogrouping_2():
    # Build test DataFrame
    df = pd.DataFrame(
        [['2020-01-01 00:00:00', 0.2],
         ['2020-01-01 01:00:00', -0.1],
         ['2020-01-01 02:00:00', 0.345],
         ['2020-01-01 03:00:00', 0.55],
         ['2020-01-01 04:00:00', 1.2],
         ['2020-01-01 05:00:00', -2.5],
         ['2020-01-01 06:00:00', 3.7],
         ['2020-01-01 07:00:00', 3.5],
         ['2020-01-01 08:00:00', 0.3],
         ['2020-01-01 09:00:00', -1.],
         ['2020-01-01 10:00:00', -2.1],
         ['2020-01-01 11:00:00', 1.1],
         ['2020-01-01 12:00:00', 1.123],
         ['2020-01-01 13:00:00', 5.3],
         ['2020-01-01 14:00:00', 5.55]],
        columns=['Date', 'Value'],
    )
    df['Date'] = pd.to_datetime(df['Date'])
    df = df.set_index('Date')

    sequenced_df = pds.sequence_df(df, [-1, 0], group_specs=[])

    target_df = pd.DataFrame(
         [['2020-01-01 01:00:00', 0.2, -0.1],
          ['2020-01-01 02:00:00', -0.1, 0.345],
          ['2020-01-01 03:00:00', 0.345, 0.55],
          ['2020-01-01 04:00:00', 0.55, 1.2],
          ['2020-01-01 05:00:00', 1.2, -2.5],
          ['2020-01-01 06:00:00', -2.5, 3.7],
          ['2020-01-01 07:00:00', 3.7, 3.5],
          ['2020-01-01 08:00:00', 3.5, 0.3],
          ['2020-01-01 09:00:00', 0.3, -1.],
          ['2020-01-01 10:00:00', -1., -2.1],
          ['2020-01-01 11:00:00', -2.1, 1.1],
          ['2020-01-01 12:00:00', 1.1, 1.123],
          ['2020-01-01 13:00:00', 1.123, 5.3],
          ['2020-01-01 14:00:00', 5.3, 5.55]],
        columns=['Date', ('Value', -1), ('Value', 0)],
    )
    target_df['Date'] = pd.to_datetime(target_df['Date'])
    target_df = target_df.set_index('Date')
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_nogrouping_3():
    # Build test DataFrame
    df = pd.DataFrame(
        [['A', 0.2],
         ['A', -0.1],
         ['A', 0.345],
         ['A', 0.55],
         ['B', 1.2],
         ['B', -2.5],
         ['B', 3.7],
         ['B', 3.5],
         ['B', 0.3],
         ['C', -1.],
         ['D', -2.1],
         ['E', 1.1],
         ['A', 1.123],
         ['A', 5.3],
         ['A', 5.55]],
        columns=['Group', 'Value'],
    )
    df = df.set_index('Group')

    sequenced_df = pds.sequence_df(df, [-1, 0])

    target_df = pd.DataFrame(
         [['A', 0.2, -0.1],
          ['A', -0.1, 0.345],
          ['A', 0.345, 0.55],
          ['B', 0.55, 1.2],
          ['B', 1.2, -2.5],
          ['B', -2.5, 3.7],
          ['B', 3.7, 3.5],
          ['B', 3.5, 0.3],
          ['C', 0.3, -1.],
          ['D', -1., -2.1],
          ['E', -2.1, 1.1],
          ['A', 1.1, 1.123],
          ['A', 1.123, 5.3],
          ['A', 5.3, 5.55]],
        columns=['Group', ('Value', -1), ('Value', 0)],
    )
    target_df = target_df.set_index('Group')
    target_df.columns = pd.MultiIndex.from_tuples(list(target_df.columns))

    pd.testing.assert_frame_equal(sequenced_df, target_df)

def test_multiindex_col_1():
    # Build test DataFrame
    df = pd.DataFrame(
        [[1,1],
         [1,2],
         [1,3],
         [1,4],
         [2,1],
         [2,2],
         [2,3],
         [3,1]],
        columns=pd.MultiIndex.from_tuples([('Category', 'Group'), ('Category', 'Value')]),
        index=[0,1,2,3,4,5,6,7]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'column', ('Category', 'Group'))])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([[('Category', 'Value')],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)

@pytest.mark.xfail
def test_tuples_col_1():
    # This test currently fails because pandas expects column index to be a multiindex if all elements are tuples.
    # The failure I ran into was this:
    # pandas_sequence/sequence.py:289: in sequence_df
    #    slice_df = temp_gbydf.shift(-lag)
    #/usr/lib/python3.9/site-packages/pandas/core/groupby/groupby.py:2980: in shift
    #    return self._get_cythonized_result(
    #/usr/lib/python3.9/site-packages/pandas/core/groupby/groupby.py:2945: in _get_cythonized_result
    #    return self._wrap_transformed_output(output)
    #/usr/lib/python3.9/site-packages/pandas/core/groupby/generic.py:1670: in _wrap_transformed_output
    #    columns._set_names(self.obj._get_axis(1 - self.axis).names)
    # ValueError: Length of names must match number of levels in MultiIndex.

    # Build test DataFrame
    df = pd.DataFrame(
        [[1,1],
         [1,2],
         [1,3],
         [1,4],
         [2,1],
         [2,2],
         [2,3],
         [3,1]],
        columns=[('Category', 'Group'), ('Category', 'Value')],
        index=[0,1,2,3,4,5,6,7]
    )

    sequenced_df = pds.sequence_df(df, [-1,0], group_specs=[('group', 'column', ('Category', 'Group'))])

    target_df = pd.DataFrame(
        [[1,2],
         [2,3],
         [3,4],
         [1,2],
         [2,3]],
        columns=pd.MultiIndex.from_product([[('Category', 'Value')],[-1,0]]),
        index=[1,2,3,5,6])

    pd.testing.assert_frame_equal(sequenced_df, target_df)
